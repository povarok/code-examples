from workers.frame_writer import FullFrameWriter, CropWriter, MongoWriter
from workers.number_detector import NumberDetector
from NomeroffNet.YoloV5Detector import Detector
from yolov5.utils.general import non_max_suppression
from yolov5.models.experimental import attempt_load
import torchvision.transforms as transform
from workers.logger import ColorLog
import torch.multiprocessing as mp
import torch.nn as nn
import torchvision
import numpy as np
import traceback
import hashlib
import random
import ctypes
import torch
import time
import cv2
import os

STREAM_URI = os.environ.get('STREAM_URI')
MONGO_URI = os.environ.get('MONGO_URI')
STREAM_NAME = os.environ.get('STREAM_NAME')
NUMBER_WORKERS = int(os.environ.get('NUMBER_WORKERS')) if os.environ.get('NUMBER_WORKERS') is not None else 1
HALF_MODE = bool(int(os.environ.get('HALF_MODE'))) if os.environ.get('HALF_MODE') else True

log = ColorLog(ColorLog.Green, 'Frame processor')


class FrameProcessor(mp.Process):
    YOLO_W, YOLO_H = 640, 480

    def __init__(self, shared_frame, frame_w, frame_h):
        mp.Process.__init__(self)
        self.shared_frame = shared_frame
        self.w = frame_w
        self.h = frame_h
        self.fps = []
        frame_bytes_size = np.ndarray((self.h, self.w, 3), dtype=np.uint8).nbytes
        self.detector_shared_frame = mp.Array(ctypes.c_byte, frame_bytes_size, lock=True)

        self.need_log = False
        self.half_mode = HALF_MODE

        self.num_workers = NUMBER_WORKERS
        self.make_workers = 1

        self.q_num_ans = []
        self.q_num_ask = []
        self.number_detectors = []

        self.q_make_ans = []
        self.q_make_ask = []
        self.make_detectors = []

        self.full_frame_writer = None
        self.full_frame_writer_q = None

        self.crop_writer_q = None
        self.crop_writer = None

        self.mongo_writer = None
        self.mongo_writer_q = None

        self.yolo_detector = None
        self.q_yolo_ask = None
        self.q_yolo_ans = None

        self.rds = None
        self.mongo = None

        self.worker_gen_make = None
        self.worker_gen_num = None

        self.device = None

        self.number_detector = None
        self.number_resize = None

        self.number_is_good_model_classes = None
        self.number_is_good_model = None
        self.number_is_good_loader = None

        self.make_classes = None
        self.model_mm = None
        self.loader_mm = None

        self.yolo_model = None
        self.yolo_names = None
        self.yolo_resize = None

    def spawn_workers(self):
        self.crop_writer_q = mp.Queue()
        self.crop_writer = CropWriter(queue=self.crop_writer_q)
        self.crop_writer.start()

        self.full_frame_writer_q = mp.Queue()
        self.full_frame_writer = FullFrameWriter(queue=self.full_frame_writer_q)
        self.full_frame_writer.start()

        self.mongo_writer_q = mp.Queue()
        self.mongo_writer = MongoWriter(queue=self.mongo_writer_q)
        self.mongo_writer.start()

        self.worker_gen_num = self.__worker_gen_num()

        for i in range(self.num_workers):
            self.q_num_ans.append(mp.Queue())
            self.q_num_ask.append(mp.Queue())
            nd = NumberDetector(shared_array=self.detector_shared_frame,
                                q_ans=self.q_num_ans[-1],
                                q_ask=self.q_num_ask[-1],
                                frame_w=self.w,
                                frame_h=self.h)
            nd.start()
            self.q_num_ans[-1].get()  # wait for warmup
            self.number_detectors.append(nd)

    def __worker_gen_num(self):
        while True:
            for i in range(self.num_workers):
                yield i

    def make_model_init(self):
        success = True
        try:
            self.make_classes = ['Audi', 'BMW', 'Chevrolet', 'Citroen', 'Daewoo', 'Ford', 'Honda', 'Hyundai',
                                 'Infinity', 'Kia', 'Lada', 'Lexus', 'Mazda', 'Mercedes', 'Mitsubishi', 'Nissan',
                                 'Opel', 'PEUGEOT', 'Porsche', 'Renault', 'Rover', 'Škoda', 'Subaru', 'Suzuki', 'Toyota',
                                 'Volkswagen', 'Volvo']
            self.model_mm = torchvision.models.resnet34(pretrained=True)
            num_ftrs = self.model_mm.fc.in_features
            self.model_mm.fc = torch.nn.Linear(num_ftrs, len(self.make_classes))
            self.model_mm = self.model_mm.to(self.device).half() if self.half_mode else self.model_mm.to(self.device)

            self.model_mm.load_state_dict(torch.load('models/model_makemodel'))
            self.model_mm.eval()

            self.loader_mm = transform.Compose([transform.Resize((300, 300)),
                                                transform.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])
        except Exception as e:
            log.print('ERROR MAKE MODEL INIT')
            log.print(e)
            success = False
        return success

    def make_model_detect(self, frame, crops):
        ts = time.time()
        images_stack = []
        for x1, y1, x2, y2, cls in crops:
            images_stack.append(self.loader_mm(frame[:, :, y1:y2, x1:x2].squeeze(0)))
        images_stack = torch.stack(images_stack)
        output = self.model_mm(images_stack)
        conf, predicted = torch.max(output.data, 1)
        detections = []
        for i, e in enumerate(predicted):
            name = self.make_classes[predicted[i].item()]
            detections.append((name, conf[i].item()))
        if self.need_log: log.print(f'Make detector time:   {time.time() - ts: >5.2f}')
        return detections

    def yolo_model_init(self):
        success = True
        try:
            self.device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
            if self.half_mode:
                log.print('LOAD HALF YOLO')
            else:
                log.print('LOAD FULL YOLO')
            self.yolo_model = attempt_load('yolov5m.pt',
                                           map_location=self.device).half() if self.half_mode else attempt_load(
                'yolov5m.pt', map_location=self.device)
            self.yolo_names = self.yolo_model.module.names if hasattr(self.yolo_model,
                                                                      'module') else self.yolo_model.names
            self.yolo_resize = transform.Resize(size=(self.YOLO_H, self.YOLO_W))
        except Exception as e:
            log.print('ERROR INIT YOLO')
            log.print(e)
            success = False
        return success

    def yolo_detect(self, frame):
        ts = time.time()
        pred = self.yolo_model(self.yolo_resize(frame))[0]
        pred = non_max_suppression(pred)
        if self.need_log: log.print(f'YOLO TIME           : {time.time() - ts: >5.2f}')

        yolo_det = []
        for i, det in enumerate(pred):
            for d in det:
                x1 = max(int(d[0].item() / self.YOLO_W * self.w), 0)
                y1 = max(int(d[1].item() / self.YOLO_H * self.h), 0)
                x2 = min(int(d[2].item() / self.YOLO_W * self.w), self.w)
                y2 = min(int(d[3].item() / self.YOLO_H * self.h), self.h)
                acc = d[4].item()
                cls = self.yolo_names[int(d[5].round().item())]
                if acc > 0.8:
                    yolo_det.append((x1, y1, x2, y2, acc, cls))
        return yolo_det

    def number_model_init(self):
        self.number_detector = Detector()
        self.number_detector.load()
        self.number_resize = transform.Resize(size=(480, 640))

    def number_detect(self, frame, crops):
        ts = time.time()
        images_stack = []
        for x1, y1, x2, y2, cls in crops:
            img = self.number_resize(frame[:, :, y1:y2, x1:x2]).squeeze(0)
            images_stack.append(img)
        images_stack = torch.stack(images_stack)
        pred = self.number_detector.model(images_stack)[0]
        predictions = non_max_suppression(pred)
        resp = []
        for k, pred in enumerate(predictions):
            greatest = None
            greatest_score = 0
            x1, y1, x2, y2, cls = crops[k]
            for i, det in enumerate(pred):
                acc = det[4].item()
                if acc > 0.85:
                    num_x1 = int(det[0].item() / 640.0 * (x2 - x1) + x1)
                    num_y1 = int(det[1].item() / 480.0 * (y2 - y1) + y1)
                    num_x2 = int(det[2].item() / 640.0 * (x2 - x1) + x1)
                    num_y2 = int(det[3].item() / 480.0 * (y2 - y1) + y1)
                    score = (num_x2 - num_x1) * (num_y2 - num_y1) * acc * acc
                    if score > greatest_score:
                        greatest_score = score
                        greatest = [num_x1, num_y1, num_x2, num_y2, acc, det[5].item()]
            resp.append(greatest)
        if self.need_log: log.print(f'Number detector time: {time.time() - ts: >5.2f}')
        return resp

    def number_is_good_model_init(self):
        self.number_is_good_model_classes = ['0', '1', '2', '3']  # 0=very bad 3=nice
        self.number_is_good_model = torchvision.models.resnet18(pretrained=True)
        num_ftrs = self.number_is_good_model.fc.in_features
        self.number_is_good_model.fc = nn.Linear(num_ftrs, len(self.number_is_good_model_classes))
        self.number_is_good_model.load_state_dict(torch.load('models/number_validator_resnet18'))
        self.number_is_good_model = self.number_is_good_model.to(self.device)
        self.number_is_good_model.eval()
        self.number_is_good_loader = transform.Compose([transform.Resize((300, 60)),
                                                        transform.Grayscale(num_output_channels=3),
                                                        transform.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))])

    def number_is_good_detect(self, images):
        ts = time.time()
        images_2 = []
        for image in images:
            frame = (torch.from_numpy(image).to(self.device)[:, :, [2, 1, 0]].transpose_(0, 1).transpose_(2, 0) / 255)
            images_2.append(self.number_is_good_loader(frame).float())
        stack = torch.stack(images_2)
        output = self.number_is_good_model(stack)
        conf, predicted = torch.max(output.data, 1)
        detections = []
        for i, e in enumerate(predicted):
            name = self.number_is_good_model_classes[predicted[i].item()]
            detections.append((name, conf[i].item()))
        if self.need_log: log.print(f'Number is good time : {time.time() - ts: >5.2f}')
        return [n == '3' and c > 1.5 for n, c in detections]

    def run(self):
        self.spawn_workers()
        log.print('WORKERS SPAWNED')
        init_done = self.yolo_model_init()
        if not init_done:
            return
        log.print("YOLO INIT DONE")
        init_done = self.make_model_init()
        if not inited:
            return
        log.print("MAKEMODEL INIT DONE")
        self.number_model_init()
        log.print("NUMBER MODEL INIT DONE")
        self.number_is_good_model_init()
        log.print("NUMBER IS GOOD INIT DONE")

        while True:
            try:
                self.need_log = random.random() < 0.05
                frame_ts = time.time()
                frame_np, frame = self.read_shared_frame()
                output_image = frame_np.copy()
                self.share_frame_to_detectors(frame_np)

                # check frame is empty
                if not frame.any():
                    continue

                # YOLO detector
                yolo_det = self.yolo_detect(frame)
                detected_objects = {(x1, y1, x2, y2, cls): {
                    'makemodel': None,
                    'makemodel_conf': 0.0,
                    'yolo': cls,
                    'object_image': f'{hashlib.md5(output_image[y1:y2, x1:x2, :].tobytes()).hexdigest()}.jpg',
                    'number': None,
                    'number_image': None,
                    'timestamp': time.time() * 1000,
                    'stream_name': int(STREAM_NAME)
                } for x1, y1, x2, y2, acc, cls in yolo_det if cls in ['car', 'bus', 'truck', 'person']}

                # Make models detector
                crops = [(x1, y1, x2, y2, cls) for x1, y1, x2, y2, acc, cls in yolo_det if cls == 'car']
                if crops:
                    make_det = self.make_model_detect(frame, crops)
                    for i, c in enumerate(crops):
                        detected_objects[c]['makemodel_conf'] = make_det[i][1]
                        detected_objects[c]['makemodel'] = make_det[i][0]

                # Draw detections
                ts = time.time()
                for yolo_d, obj_dict in detected_objects.items():
                    x1, y1, x2, y2, cls = yolo_d

                    if cls == 'person':  # Draw red rect on persons
                        cv2.rectangle(output_image, (x1, y1), (x2, y2), (0, 0, 255), 10)

                    if cls in ['car', 'bus', 'truck']:  # Draw blue rect on cars
                        cv2.rectangle(output_image, (x1, y1), (x2, y2), (255, 0, 0), 10)

                    if obj_dict['makemodel_conf'] > 8.0:
                        cv2.putText(output_image, obj_dict['makemodel'],
                                    (int(x1 + 50), int(y1 + 100)), cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 255), 25)
                        cv2.putText(output_image, obj_dict['makemodel'],
                                    (int(x1 + 50), int(y1 + 100)), cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 0), 2)
                if self.need_log: log.print(f'Draw rects          : {time.time() - ts: >5.2f}')

                # Numbers detector
                worker_ids_num = []
                crops = [(x1, y1, x2, y2, cls) for x1, y1, x2, y2, acc, cls in yolo_det if
                         cls in ['car', 'bus', 'truck']]
                if crops:
                    num_det = self.number_detect(frame, crops)
                    ts = time.time()

                    crops_with_num_ids = [i for i, nd in enumerate(num_det) if nd is not None]
                    target_boxes = [nd for nd in num_det if nd is not None]
                    for tb in target_boxes:
                        # TODO is horizontal check
                        worker_id = next(self.worker_gen_num)
                        worker_ids_num.append(worker_id)
                        self.q_num_ask[worker_id].put(tb)

                    num_det = []
                    for w_id in worker_ids_num:
                        tb, num_t_det = self.q_num_ans[w_id].get()
                        if num_t_det:
                            num_det.append(num_t_det[0])

                    zones = [nd[2] for nd in num_det]
                    if zones:
                        good_zones = self.number_is_good_detect(zones)
                        for i, gz in enumerate(good_zones):
                            num_det[i][4] = good_zones[i]

                    if self.need_log: log.print(
                        f'Numbers x {len(worker_ids_num): >3} :       {time.time() - ts: >5.2f}')

                    ts = time.time()
                    for i, cwni in enumerate(crops_with_num_ids):
                        num_box, num_points, num_image, num_text, num_is_good = num_det[i]
                        if num_is_good:
                            detected_objects[crops[cwni]]['number'] = num_text
                            detected_objects[crops[cwni]]['number_image'] = detected_objects[crops[cwni]][
                                'object_image'].replace('.jpg', '_n.jpg')
                            self.crop_writer_q.put((detected_objects[crops[cwni]]['number_image'], num_image, True))
                    if self.need_log: log.print(f'Write plates        : {time.time() - ts: >5.2f}')

                    ts = time.time()
                    for num_box, num_points, num_image, num_text, num_is_good in num_det:
                        num_x1, num_y1, num_x2, num_y2, acc, b = tuple(num_box)
                        num_w = num_x2 - num_x1

                        cv2.rectangle(output_image,
                                      (num_x1, num_y1),
                                      (num_x2, num_y2),
                                      (0, 255, 0) if num_is_good else (0, 0, 255), 10)

                        z = cv2.resize(num_image, (300, 100), cv2.INTER_CUBIC)
                        x_offset = int(num_w / 2.0) - 150
                        if num_x1 + z.shape[0] > output_image.shape[0]:
                            z = z[:output_image.shape[0] - z.shape[0] - num_y2, :, :]

                        xx1 = num_x1 + x_offset
                        xx2 = num_x1 + z.shape[1] + x_offset
                        if xx1 < 0:
                            xx2 -= xx1
                            xx1 -= xx1

                        if xx2 > output_image.shape[1]:
                            xx1 += output_image.shape[1] - xx2
                            xx2 += output_image.shape[1] - xx2
                        try:
                            output_image[num_y2:num_y2 + z.shape[0], xx1:xx2] = cv2.cvtColor(z, cv2.COLOR_BGR2RGB)
                        except Exception as e:
                            log.print('Error draw plate')
                            log.print(e)

                        if num_is_good:
                            # draw text
                            cv2.putText(output_image, f'{num_text}', (num_x1 + x_offset, num_y2 + z.shape[0] + 50),
                                        cv2.FONT_HERSHEY_SIMPLEX, 2, (255, 255, 255), 50)
                            cv2.putText(output_image, f'{num_text}', (num_x1 + x_offset, num_y2 + z.shape[0] + 50),
                                        cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 0), 5)
                    if self.need_log: log.print(f'Draw plates         : {time.time() - ts: >5.2f}')

                ts = time.time()
                self.full_frame_writer_q.put(output_image)
                for k, v in detected_objects.items():
                    if v['number'] or v['makemodel']:
                        x1, y1, x2, y2, cls = k
                        yolo_object_crop = frame_np[y1:y2, x1:x2, :]
                        self.crop_writer_q.put(
                            (detected_objects[(x1, y1, x2, y2, cls)]['object_image'], yolo_object_crop, True))
                        if v['number']:
                            self.crop_writer_q.put((v['number'], yolo_object_crop, False))
                        self.mongo_writer_q.put(v)
                if self.need_log: log.print(f'Writes time         : {time.time() - ts: >5.2f}')

                self.fps.append(time.time())
                self.fps = self.fps[-20:]
                if self.need_log:
                    log.print(f'----------------------------')
                    log.print(f'Frame time          : {time.time() - frame_ts: >5.2f}')
                    if self.fps[-1] - self.fps[0] != 0:
                        log.print(f'FPS                 : {len(self.fps) / (self.fps[-1] - self.fps[0]): >5.2f}')
                    log.print(f'----------------------------')
            except Exception as e:
                log.print(e)
                traceback.print_exc()

    def read_shared_frame(self):
        with self.shared_frame.get_lock():
            f1 = np.ctypeslib.as_array(self.shared_frame.get_obj())
            frame = np.ndarray((self.w * self.h * 3), dtype=np.uint8)
            frame[:] = f1[:]
        frame_np = frame.reshape((self.h, self.w, 3))
        tensor_frame = (
                    torch.from_numpy(frame).to(self.device).resize_(self.h, self.w, 3)[:, :, [2, 1, 0]].transpose_(0,
                                                                                                                   1).transpose_(
                        2, 0).unsqueeze(0) / 255).float().half() if self.half_mode else (
                    torch.from_numpy(frame).to(self.device).resize_(self.h, self.w, 3)[:, :, [2, 1, 0]].transpose_(0,
                                                                                                                   1).transpose_(
                        2, 0).unsqueeze(0) / 255).float()
        frame = tensor_frame
        return frame_np, frame

    def share_frame_to_detectors(self, frame):
        with self.detector_shared_frame.get_lock():
            np_array = np.ctypeslib.as_array(self.detector_shared_frame.get_obj())
            np_array[:] = frame.reshape((self.h * self.w * 3))[:]
