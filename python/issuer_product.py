import traceback

from django.db.models import OuterRef, Subquery, Sum
from django.views.decorators.csrf import csrf_exempt
from django.core.exceptions import PermissionDenied
from app.api.auth import is_auth, options
from app.utils import resize_image_file
from django.db.models import Prefetch
from django.http import JsonResponse
from app.api.auth import is_auth
from django.db.models import Q
from dateutil import parser
from app.models import *
import datetime
import pickle
import math
import json
import pytz
import copy
import csv
import io


@csrf_exempt
@is_auth()
@options(methods=["GET"])
def issuer_products_by_ids(request):
    ids      = [int(i) for i in request.GET.get('ids', '').split(',')]
    order_by = request.GET.get('order', 'order,-create_at')

    f = {'id__in': ids}
    q = IssuerProduct.objects
    q = q.filter(**f)
    q = IssuerProduct.prefetch(q)
    q = q.order_by(*order_by.split(','))

    resp = [obj.to_dict() for obj in q.all()]
    return JsonResponse(resp, safe=False)


@csrf_exempt
@is_auth()
@options(methods=["GET", "DELETE"])
def issuer_products(request):
    if request.method == 'GET':
        category           = int(request.GET.get('category', 0))
        categories         = [int(_pk) for _pk in request.GET.get('categories', '').split(',') if _pk != '']
        if len(categories) == 1:
            category = categories[0]
            categories = None
        creator            = int(request.GET.get('creator', 0))
        issuer_firm        = int(request.GET.get('issuer_firm', 0))
        not_in_seller_firm = int(request.GET.get('not_in_seller_firm', 0))
        order_by           = request.GET.get('order', 'order,-create_at')
        with_expired       = bool(int(request.GET.get('with_expired', 0)))
        approved_only      = bool(int(request.GET.get('approved_only', 1)))
        with_sellers       = bool(int(request.GET.get('with_sellers', 0)))
        only_for_me        = bool(int(request.GET.get('only_for_me', 1)))
        name               = request.GET.get('name')
        f = {}
        e = {}

        if categories:
            f['category__pk__in'] = categories
        elif category:
            f['category__pk'] = category
        if creator:
            f['creator__pk'] = creator
        if issuer_firm:
            f['issuer_firm__pk'] = issuer_firm
        if approved_only:
            f['approved'] = True
        if name:
            f['name__icontains'] = name
        if only_for_me:
            seller_firms_where_i_am_client = ImportedClient.objects.select_related('seller_firm').filter(user=request.user).values_list("seller_firm__pk", flat=True)
            target_issuer_product_pk = SellerProduct.objects.select_related('seller_firm').filter(seller_firm__pk__in=seller_firms_where_i_am_client).values_list("product__pk", flat=True)
            f['pk__in'] = target_issuer_product_pk
        if not_in_seller_firm:
            e['pk__in'] = [i.product.pk for i in SellerProduct.objects.select_related('product').filter(seller_firm__pk=not_in_seller_firm).all()]
        if not with_expired:
            e['expire_date__lt'] = pytz.UTC.localize(datetime.datetime.now())

        q = IssuerProduct.objects
        if f:
            q = q.filter(**f)
        if e:
            qe = None
            for k, v in e.items():
                if qe is None:
                    qe = Q(**{k: v})
                else:
                    qe.add(Q(**{k: v}), Q.OR)
            q = q.exclude(qe)

        if with_sellers:
            if only_for_me:
                q = q.prefetch_related(Prefetch('sellerproduct_set', queryset=SellerProduct.objects.select_related('seller_firm').filter(seller_firm__pk__in=seller_firms_where_i_am_client)))
            else:
                q = q.prefetch_related(Prefetch('sellerproduct_set', queryset=SellerProduct.objects.select_related('seller_firm')))

        q = IssuerProduct.prefetch(q)
        q = q.order_by(*order_by.split(','))
        resp = [obj.to_dict_with_sellers() if with_sellers else obj.to_dict() for obj in q.all()]
        return JsonResponse(resp, safe=False)

    elif request.method == 'DELETE':
        req = json.loads(request.body)
        IssuerProduct.objects.filter(pk__in=req['ids']).delete()
        return JsonResponse({}, safe=False)
    return JsonResponse({}, safe=False)


@csrf_exempt
@is_auth()
@options(methods=["GET", "POST", "PATCH", "DELETE"])
def issuer_product(request, pk=None):
    if request.method == "GET":
        with_sellers = bool(int(request.GET.get('with_sellers', 0)))
        only_for_me  = bool(int(request.GET.get('only_for_me', 1)))

        q = IssuerProduct.objects
        if with_sellers:
            if only_for_me:
                seller_firms_where_i_am_client = ImportedClient.objects.select_related('seller_firm').filter(user=request.user).values_list("seller_firm__pk", flat=True)
                q = q.prefetch_related(Prefetch('sellerproduct_set', queryset=SellerProduct.objects.select_related('seller_firm').filter(seller_firm__pk__in=seller_firms_where_i_am_client).distinct('seller_firm')))
            else:
                q = q.prefetch_related(Prefetch('sellerproduct_set'))
                q = q.prefetch_related(Prefetch('sellerproduct_set__seller_firm'))
        obj = q.get(pk=pk)
        return JsonResponse(obj.to_dict_with_sellers() if with_sellers else obj.to_dict())

    elif request.method == "POST":
        req = json.loads(request.body)
        obj = IssuerProduct()
        for k, v in req.items():
            if k in ('pk', 'create_at', 'update_at', 'creator'):
                continue
            elif k in ('open_date', 'close_date', 'expire_date') and v is not None:
                v = datetime.datetime.fromtimestamp(v)
            elif k in ('categories', ):
                k = 'category_id'
            if k == 'issuer_firm':
                v = IssuerFirm.objects.get(pk=v)
            setattr(obj, k, v)

        if 'issuer_firm' not in req:
            if request.user is not None and request.user.issuer_firm is not None:
                obj.issuer_firm = request.user.issuer_firm
            else:
                raise PermissionDenied()

        obj.creator = request.user
        obj.save()
        obj.refresh_from_db()
        return JsonResponse(obj.to_dict(), safe=False)

    elif request.method == 'PATCH':
        obj = IssuerProduct.objects.get(pk=pk)
        req = json.loads(request.body)
        for k, v in req.items():
            if k in ('pk', 'create_at', 'update_at', 'creator'):
                continue
            elif k in ('open_date', 'close_date', 'expire_date') and v is not None:
                v = datetime.datetime.fromtimestamp(v)
            if k == 'categories':
                k = 'category_id'
            if k == 'issuer_firm':
                v = IssuerFirm.objects.get(pk=v)
            setattr(obj, k, v)
        obj.save()

        obj.refresh_from_db()
        return JsonResponse(obj.to_dict(), safe=False)

    elif request.method == 'DELETE':
        IssuerProduct.objects.filter(pk=pk).delete()
        return JsonResponse({}, safe=False)


@csrf_exempt
@is_auth()
@options(methods=["GET"])
def issuer_product_sellers(request, pk: int):
    only_for_me = bool(int(request.GET.get('only_for_me', 1)))

    q = SellerProduct.objects.filter(product__pk=pk)
    if only_for_me:
        seller_firms_where_i_am_client = ImportedClient.objects.select_related('seller_firm').filter(user=request.user).values_list("seller_firm__pk", flat=True)
        q = q.filter(seller_firm__pk__in=seller_firms_where_i_am_client)

    q = q.select_related('product', 'seller_firm', 'product__creator', 'product__issuer_firm')
    return JsonResponse([obj.to_dict_short() for obj in q], safe=False)


@csrf_exempt
@options(methods=["GET", "POST", "PATCH", "DELETE"])
def issuer_product_image(request, pk=None):
    if request.method == "GET":
        obj = IssuerProductImage.objects.get(pk=pk)
        return JsonResponse(obj.to_dict())

    elif request.method == "POST":
        product_id = int(request.GET.get('product'))
        order = int(request.GET.get('order', 1))
        background_color = request.GET.get('background_color', 'eeeeee')
        obj = IssuerProduct.objects.get(pk=product_id)
        image = request.FILES['file']
        obj_img = IssuerProductImage(product=obj, image=image, order=order, background_color=background_color)
        obj_img.save()
        obj_img.refresh_from_db()
        return JsonResponse(obj_img.to_dict(), safe=True)

    elif request.method == 'PATCH':
        req = json.loads(request.body)
        obj = IssuerProductImage.objects.get(pk=pk)
        if 'background_color' in req and req['background_color'] != obj.background_color:
            obj.background_color = req['background_color']
            obj.save(update_fields=['background_color'])
        if 'order' in req and req['order'] != obj.order:
            obj.order = req['order']
            obj.save(update_fields=['order'])
        obj.refresh_from_db()
        return JsonResponse(obj.to_dict(), safe=True)

    elif request.method == 'DELETE':
        obj = IssuerProductImage.objects.get(pk=pk)
        obj.delete()
        return JsonResponse({}, safe=False)


@csrf_exempt
@options(methods=["GET", "POST", "PATCH", "DELETE"])
def issuer_product_video(request, pk=None):
    if request.method == "GET":
        obj = IssuerProductVideo.objects.get(pk=pk)
        return JsonResponse(obj.to_dict())

    elif request.method == "POST":
        product_id = int(request.GET.get('product'))
        order = int(request.GET.get('order', 1))
        background_color = request.GET.get('background_color', '000000')
        q = {
            'video': request.FILES['file'],
            'order': order,
            'background_color': background_color,
            'product': IssuerProduct.objects.get(pk=product_id)
        }
        if 'preview' in request.FILES:
            q['preview'] = request.FILES['preview']

        obj_img = IssuerProductVideo(**q)
        obj_img.save()
        obj_img.refresh_from_db()
        return JsonResponse(obj_img.to_dict())

    elif request.method == 'PATCH':
        req = json.loads(request.body)
        obj = IssuerProductVideo.objects.get(pk=pk)
        if 'order' in req and req['order'] != obj.order:
            obj.order = req['order']
            obj.save(update_fields=['order'])
        if 'background_color' in req and req['background_color'] != obj.background_color:
            obj.background_color = req['background_color']
            obj.save(update_fields=['background_color'])
        obj.refresh_from_db()
        return JsonResponse(obj.to_dict(), safe=True)

    elif request.method == 'DELETE':
        obj = IssuerProductVideo.objects.get(pk=pk)
        obj.delete()
        return JsonResponse({}, safe=False)


@csrf_exempt
@options(methods=["POST"])
def issuer_product_video_preview(request, pk=None):
    obj = IssuerProductVideo.objects.get(pk=pk)
    obj.preview = request.FILES['file']
    obj.save(update_fields=['preview'])
    return JsonResponse(obj.to_dict())


@csrf_exempt
@options(methods=["GET", "POST", "PATCH", "DELETE"])
def issuer_product_attachment(request, pk=None):
    if request.method == "GET":
        obj = IssuerProductAttachment.objects.get(pk=pk)
        return JsonResponse(obj.to_dict())

    elif request.method == "POST":
        product_id = int(request.GET.get('product'))
        order = int(request.GET.get('order', 1))
        obj = IssuerProduct.objects.get(pk=product_id)
        attachment = request.FILES['file']
        obj_attachment = IssuerProductAttachment(product=obj, attachment=attachment, order=order)
        obj_attachment.save()
        obj_attachment.refresh_from_db()
        return JsonResponse(obj_attachment.to_dict(), safe=True)

    elif request.method == 'PATCH':
        req = json.loads(request.body)
        obj = IssuerProductAttachment.objects.get(pk=pk)
        if 'order' in req and req['order'] != obj.order:
            obj.order = req['order']
            obj.save(update_fields=['order'])
        obj.refresh_from_db()
        return JsonResponse(obj.to_dict(), safe=True)

    elif request.method == 'DELETE':
        obj = IssuerProductAttachment.objects.get(pk=pk)
        obj.delete()
        return JsonResponse({}, safe=False)


@csrf_exempt
@options(methods=["GET", "POST", "PATCH", "DELETE"])
def issuer_product_chart(request, pk=None):
    if request.method == "GET":
        obj = IssuerProductChart.objects.get(pk=pk)
        return JsonResponse(obj.to_dict())

    elif request.method == "POST":
        try:
            obj = IssuerProductChart()
            for k, v in request.GET.items():
                if k in ['pk', 'update_at', 'create_at']:
                    continue
                if k == 'product':
                    v = IssuerProduct.objects.get(pk=v)
                setattr(obj, k, v)

            file_bytes = b''
            f = request.FILES['file']
            for chunk in f.chunks():
                file_bytes += chunk

            csv_string = io.StringIO(file_bytes.decode('utf-8'))
            csv_string.seek(0)
            reader = csv.reader(csv_string)

            xs = []
            ys = []
            x_type = request.GET.get('x_type')
            y_type = request.GET.get('y_type')
            chart_type = request.GET.get('chart_type')
            y_incremental = bool(int(request.GET.get('y_incremental', 0)))

            first_row = True
            has_header = False
            for row in reader:
                if first_row:
                    if row[0] == 'X' and row[1] == 'Y':
                        has_header = True
                        continue
                    first_row = False

                print(row, flush=True)
                xv = row[0]
                if x_type == 'date':
                    xv = parser.parse(row[0]).timestamp()
                elif x_type == 'float':
                    xv = float(row[0])
                elif x_type == 'string':
                    xv = str(row[0])
                xs.append(xv)

                yv = row[1]
                if y_type == 'date':
                    yv = parser.parse(row[1]).timestamp()
                elif y_type == 'float':
                    yv = float(row[1])
                elif y_type == 'string':
                    yv = str(row[1])

                if y_incremental and y_type == 'float':
                    prev_val = 100.0
                    if len(ys) > 0:
                        prev_val = ys[-1]
                    yv = prev_val * (100.0 + yv) / 100.0

                ys.append(yv)

            if not y_incremental and chart_type == 'scatter' and not has_header and y_type == 'float':
                delimiter = float(ys[0])
                ys = [_y / delimiter for _y in ys]

            obj.chart = pickle.dumps({'x': xs, 'y': ys})
            obj.save()
            obj.refresh_from_db()
            return JsonResponse(obj.to_dict(), safe=True)
        except Exception as e:
            return JsonResponse({'error': f'{e.__class__.__name__}: {str(e)}'}, safe=True)

    elif request.method == 'PATCH':
        req = json.loads(request.body)
        obj = IssuerProductChart.objects.get(pk=pk)

        update_fields = []
        for k, v in req.items():
            if k in ['pk', 'x_type', 'y_type', 'chart_type', 'chart', 'product', 'create_at', 'update_at']:
                continue
            update_fields.append(k)
            setattr(obj, k, v)
        if update_fields:
            obj.save(update_fields=update_fields)
            obj.refresh_from_db()

        return JsonResponse(obj.to_dict())

    elif request.method == 'DELETE':
        obj = IssuerProductChart.objects.get(pk=pk)
        obj.delete()
        return JsonResponse({}, safe=False)


@csrf_exempt
@options(methods=["POST"])
def issuer_product_reorder(request):
    req = json.loads(request.body)
    resp = {}
    for _id, order in req.items():
        try:
            obj = IssuerProduct.objects.get(pk=int(_id))
            if obj.order != order:
                obj.order = order
                obj.save(update_fields=['order'])
            resp[_id] = order
        except Exception as e:
            print(e, flush=True)
            resp[_id] = str(e)
    return JsonResponse(resp)


@csrf_exempt
@options(methods=["POST"])
def issuer_product_image_reorder(request):
    req = json.loads(request.body)
    resp = {}
    for image_id, order in req.items():
        try:
            obj = IssuerProductImage.objects.get(pk=int(image_id))
            if obj.order != order:
                obj.order = order
                obj.save(update_fields=['order'])
            resp[image_id] = order
        except Exception as e:
            print(e, flush=True)
            resp[image_id] = str(e)
    return JsonResponse(resp)


@csrf_exempt
@options(methods=["POST"])
def issuer_product_video_reorder(request):
    req = json.loads(request.body)
    resp = {}
    for video_id, order in req.items():
        try:
            obj = IssuerProductVideo.objects.get(pk=int(video_id))
            if obj.order != order:
                obj.order = order
                obj.save(update_fields=['order'])
            resp[video_id] = order
        except Exception as e:
            print(e, flush=True)
            resp[video_id] = str(e)
    return JsonResponse(resp)


@csrf_exempt
@options(methods=["POST"])
def issuer_product_attachment_reorder(request):
    req = json.loads(request.body)
    resp = {}
    for attachment_id, order in req.items():
        try:
            obj = IssuerProductAttachment.objects.get(pk=int(attachment_id))
            if obj.order != order:
                obj.order = order
                obj.save(update_fields=['order'])
            resp[attachment_id] = order
        except Exception as e:
            print(e, flush=True)
            resp[attachment_id] = str(e)
    return JsonResponse(resp)


@csrf_exempt
@options(methods=["POST"])
def issuer_product_chart_reorder(request):
    req = json.loads(request.body)
    resp = {}
    for chart_id, order in req.items():
        try:
            obj = IssuerProductChart.objects.get(pk=int(chart_id))
            if obj.order != order:
                obj.order = order
                obj.save(update_fields=['order'])
            resp[chart_id] = order
        except Exception as e:
            print(e, flush=True)
            resp[chart_id] = str(e)
    return JsonResponse(resp, safe=False)
