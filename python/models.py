from django.utils.translation import gettext_lazy as _
from django.contrib.auth.models import AbstractUser
from django.db.models import UniqueConstraint
from django.db.models import Count, Sum
from django.db.models import Prefetch
from botocore.client import Config
from django.db import models
from babel import numbers
import datetime
import hashlib
import pickle
import boto3
import uuid
import pytz
import os


s3 = boto3.resource('s3',
                    endpoint_url=f"http://minio:9000",
                    aws_access_key_id=os.getenv("MINIO_ACCESS_KEY"),
                    aws_secret_access_key=os.getenv("MINIO_SECRET_KEY"),
                    config=Config(signature_version='s3v4'),
                    region_name='us-east-1')
S3_BUCKET = os.getenv("S3_BUCKET")
STORAGE_URL = os.getenv("STORAGE_URL")



class Category(models.Model):
    class Meta:
        verbose_name_plural = "Categories"

    name = models.CharField(max_length=200)
    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'<Category[id:{self.pk}, name:{self.name}]>'

    def to_dict(self):
        return {
            'id': self.pk,
            'name': self.name
        }

class IssuerProduct(models.Model):
    name               = models.CharField(max_length=200)
    creator            = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True)
    issuer_firm        = models.ForeignKey(IssuerFirm, on_delete=models.CASCADE)
    short_description  = models.TextField(blank=True)
    description        = models.TextField(blank=True)
    params             = models.JSONField(null=True, blank=True)
    price              = models.FloatField(default=0.0)
    price_name         = models.CharField(max_length=20, default='Min. Investment')
    category           = models.ForeignKey(Category, on_delete=models.CASCADE)
    capacity_available = models.BigIntegerField(null=True, blank=True)
    approved           = models.BooleanField(default=False)
    expire_date        = models.DateTimeField(null=True, blank=True)
    order              = models.IntegerField(null=True, blank=True)

    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    @property
    def locale_price(self) -> str:
        return numbers.format_decimal(self.price, locale='en_US')

    @property
    def time_left(self):
        if self.expire_date:
            return self.expire_date - pytz.UTC.localize(datetime.datetime.now())
        else:
            return None

    @staticmethod
    def prefetch(q):
        q = q.select_related('creator')
        q = q.select_related('issuer_firm')
        q = q.select_related('category')
        q = q.prefetch_related(Prefetch('issuerproductimage_set', queryset=IssuerProductImage.objects.order_by('order')))
        q = q.prefetch_related(Prefetch('issuerproductattachment_set', queryset=IssuerProductAttachment.objects.order_by('order')))
        q = q.prefetch_related(Prefetch('issuerproductchart_set', queryset=IssuerProductChart.objects.order_by('order')))
        q = q.prefetch_related(Prefetch('issuerproductvideo_set', queryset=IssuerProductVideo.objects.order_by('order')))
        q = q.prefetch_related(Prefetch('like_set', queryset=Like.objects.all()))
        return q

    def __str__(self):
        return f'<Product[id:{self.pk}, name:{self.name}]>'

    def to_dict(self):
        return {
            'id': self.pk,
            'name': self.name,
            'creator': self.creator.to_dict_preview() if self.creator else None,
            'issuer_firm': self.issuer_firm.to_dict_preview() if self.issuer_firm else None,
            'short_description': self.short_description,
            'description': self.description,
            'params': self.params,
            'price':  self.price,
            'price_name': self.price_name,
            'categories':  [self.category.to_dict()],
            'images':      [i.to_dict() for i in self.issuerproductimage_set.all()],
            'attachments': [i.to_dict() for i in self.issuerproductattachment_set.all()],
            'charts':      [i.to_dict() for i in self.issuerproductchart_set.all()],
            'videos':      [i.to_dict() for i in self.issuerproductvideo_set.all()],
            'like_count':  self.like_set.count(),
            'capacity_available': self.capacity_available,
            'approved':    self.approved,
            'expire_date': self.expire_date.timestamp() if self.expire_date else None,
            'time_left':   round(self.time_left.total_seconds(), 2) if self.time_left else None,
            'order':       self.order,
            'create_at':   self.create_at.timestamp()
        }

    def to_dict_with_sellers(self):
        d = self.to_dict()
        d['seller_firms'] = []
        for sp in self.sellerproduct_set.all():
            r = sp.seller_firm.to_dict()
            r['seller_product_id'] = sp.pk
            d['seller_firms'].append(r)
        return d


class IssuerProductImage(models.Model):
    S3_PREFIX = 'product'

    product = models.ForeignKey(IssuerProduct, on_delete=models.CASCADE)
    url        = models.CharField(max_length=200, blank=True)
    image      = models.FileField(blank=False, null=True)
    order      = models.IntegerField(default=1)
    background_color = models.CharField(max_length=6, default="eeeeee", blank=True)

    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'<IssuerProductImage[id:{self.pk}, url:{self.url}]>'

    def to_dict(self):
        return {
            'id'              : self.pk,
            'url'             : self.url,
            'order'           : self.order,
            'background_color': self.background_color
        }

    def delete_image(self):
        s3.Object(S3_BUCKET, self.image.name).delete()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.pk is not None:
            old_obj: IssuerProductImage = IssuerProductImage.objects.get(pk=self.pk)

            if self.image not in (None, '', [], (), {}):
                if old_obj.image not in (None, '', [], (), {}) and old_obj.image != self.image:
                    old_obj.delete_image()

        if not self.image.name.startswith(f'{self.S3_PREFIX}/{self.product.pk}/'):
            name_prefix = hashlib.md5(str(uuid.uuid4()).encode('utf-8')).hexdigest()[:6]
            self.image.name = f'{self.S3_PREFIX}/{self.product.pk}/{name_prefix}-{self.image.name}'
            print(f'New image name: {self.image.name}', flush=True)

        super().save()
        self.url      = f'{STORAGE_URL}/{S3_BUCKET}/{self.image.name}'
        super().save()

    def delete(self, using=None, keep_parents=False):
        if self.image not in (None, '', [], (), {}):
            self.delete_image()
        super().delete()


class IssuerProductVideo(models.Model):
    S3_PREFIX = 'video'

    url         = models.CharField(max_length=200, blank=True)
    url_preview = models.CharField(max_length=200, blank=True)
    product     = models.ForeignKey(IssuerProduct, on_delete=models.CASCADE)
    video       = models.FileField(blank=False, null=True)
    preview     = models.FileField(blank=False, null=True)
    order       = models.IntegerField(default=1)
    background_color = models.CharField(max_length=6, default="eeeeee", blank=True)

    create_at = models.DateTimeField(auto_now_add=True)
    update_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'<IssuerProductVideo[id:{self.pk}, url:{self.url}]>'

    def to_dict(self):
        return {
            'id': self.pk,
            'url': self.url,
            'url_preview': self.url_preview,
            'order': self.order,
            'background_color': self.background_color
        }

    def delete_video(self):
        s3.Object(S3_BUCKET, self.video.name).delete()

    def delete_preview(self):
        s3.Object(S3_BUCKET, self.preview.name).delete()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.pk is not None:
            old_obj: IssuerProductVideo = IssuerProductVideo.objects.get(pk=self.pk)
            if self.video not in (None, '', [], (), {}):
                if old_obj.video not in (None, '', [], (), {}) and old_obj.video != self.video:
                    old_obj.delete_video()
            if self.preview not in (None, '', [], (), {}):
                if old_obj.preview not in (None, '', [], (), {}) and old_obj.preview != self.preview:
                    old_obj.delete_preview()

        if not self.video.name.startswith(f'{self.S3_PREFIX}/{self.product.pk}/'):
            name_prefix = hashlib.md5(str(uuid.uuid4()).encode('utf-8')).hexdigest()[:6]
            self.video.name = f'{self.S3_PREFIX}/{self.product.pk}/{name_prefix}-{self.video.name}'
            print(f'New video name: {self.video.name}', flush=True)

        if not self.preview.name.startswith(f'{self.S3_PREFIX}/{self.product.pk}/'):
            name_prefix = hashlib.md5(str(uuid.uuid4()).encode('utf-8')).hexdigest()[:6]
            self.preview.name = f'{self.S3_PREFIX}/{self.product.pk}/{name_prefix}-{self.preview.name}'
            print(f'New preview name: {self.preview.name}', flush=True)

        super().save()
        self.url         = f'{STORAGE_URL}/{S3_BUCKET}/{self.video.name}'
        self.url_preview = f'{STORAGE_URL}/{S3_BUCKET}/{self.preview.name}'
        super().save()

    def delete(self, using=None, keep_parents=False):
        if self.video not in (None, '', [], (), {}):
            self.delete_video()

        if self.preview not in (None, '', [], (), {}):
            self.delete_preview()

        super().delete()


class IssuerProductAttachment(models.Model):
    S3_PREFIX = 'attachment'

    url        = models.CharField(max_length=200, blank=True)
    product    = models.ForeignKey(IssuerProduct, on_delete=models.CASCADE)
    attachment = models.FileField(blank=False, null=True)
    order      = models.IntegerField(default=1)

    create_at  = models.DateTimeField(auto_now_add=True)
    update_at  = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'<IssuerProductAttachment[id:{self.pk}, url:{self.url}]>'

    def to_dict(self):
        return {
            'id': self.pk,
            'url': self.url,
            'order': self.order,
            'name': '_'.join(self.attachment.name.split('/')[2:])
        }

    def delete_attachment(self):
        s3.Object(S3_BUCKET, self.attachment.name).delete()

    def save(self, force_insert=False, force_update=False, using=None, update_fields=None):
        if self.pk is not None and self.attachment not in (None, '', [], (), {}):
            old_obj: IssuerProductAttachment = IssuerProductAttachment.objects.get(pk=self.pk)
            if old_obj.attachment not in (None, '', [], (), {}) and old_obj.attachment != self.attachment:
                old_obj.delete_attachment()

        if not self.attachment.name.startswith(f'{self.S3_PREFIX}/{self.product.pk}/'):
            name_prefix = hashlib.md5(str(uuid.uuid4()).encode('utf-8')).hexdigest()[:6]
            self.attachment.name = f'{self.S3_PREFIX}/{self.product.pk}/{name_prefix}-{self.attachment.name}'
            print(f'New attachment name: {self.attachment.name}', flush=True)
        super().save()
        self.url = f'{STORAGE_URL}/{S3_BUCKET}/{self.attachment.name}'
        super().save()

    def delete(self, using=None, keep_parents=False):
        if self.attachment not in (None, '', [], (), {}):
            self.delete_attachment()
        super().delete()


class IssuerProductChart(models.Model):
    CHART_TYPES = [
        ('scatter', 'scatter'),
        ('pie', 'pie'),
        ('bar', 'bar'),
    ]
    DATA_TYPES = [
        ('float', 'float'),
        ('date', 'date'),
        ('string', 'string'),
    ]
    title        = models.CharField(max_length=200)
    x_label      = models.CharField(max_length=200)
    x_type       = models.CharField(max_length=200, choices=DATA_TYPES, default='date')
    y_label      = models.CharField(max_length=200)
    y_type       = models.CharField(max_length=200, choices=DATA_TYPES, default='float')
    chart_type   = models.CharField(max_length=200, choices=CHART_TYPES, default='scatter')
    chart        = models.BinaryField(blank=True)
    product      = models.ForeignKey(IssuerProduct, on_delete=models.CASCADE)
    order        = models.IntegerField(default=1)
    create_at    = models.DateTimeField(auto_now_add=True)
    update_at    = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f'<IssuerProductChart[id:{self.pk}, title:{self.title}]>'

    def to_dict(self):
        return {
            'id'        : self.pk,
            'title'     : self.title,
            'x_label'   : self.x_label,
            'x_type'    : self.x_type,
            'y_label'   : self.y_label,
            'y_type'    : self.y_type,
            'order'     : self.order,
            'chart_type': self.chart_type,
            'chart'     : pickle.loads(self.chart)
        }
