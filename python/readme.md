# frame_processor.py
Car object, make, model and number detection pipeline

- Python
- Numpy
- OpenCV
- PyTorch

# issuer_product.py and models.py
Django model CRUD endpoints and specific endpoints for FK fields

- Python
- Django

