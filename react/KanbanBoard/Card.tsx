import { DeleteOutlined, FolderAddOutlined } from '@ant-design/icons'
import { Dropdown, Menu, Popconfirm } from 'antd'
import classNames from 'classnames'
import React from 'react'

import { ReactComponent as Archive } from 'assets/img/archive.svg'
import { prettyDate } from 'utils'
import UITexts from 'ui/UITexts'
import { store } from 'store'
import { api } from 'api'

import { BoardCard, Subscribe } from './boardTypes'
import styles from './KanbanBoard.module.css'

type Props = {
  card: BoardCard
  parentColumn: number
  onCardClick: (subscribe: Subscribe) => void
  onCardMouseDown: () => void
  onCardMouseUp: () => void
}

const CardOverlay: React.FC<{id: number}> = ({id})=> {
  const items = [
    {
      label: 'Archive',
      icon: <FolderAddOutlined className={styles.cardContextItemIcon} />,
      onClick: (id: number) => {
        store.dispatch(
          api.endpoints.updateSubscribe.initiate({ id: id, data: { is_archived: true } }),
        )
      },
    },
    {
      label: 'Delete',
      icon: <DeleteOutlined className={styles.cardContextItemIcon} />,
      onClick: (id: number) => {
        store.dispatch(api.endpoints.deleteSubscribe.initiate(id))
      },
    },
  ]
  return (
    <Menu className={styles.cardContextItemsContainer}>
      {items.map((e) => (
        <Menu.Item key={e.label} className={styles.cardContextItemContainer} onClick={() => e.onClick(id)}>
          <div className={styles.cardContextItem}>
            {e.icon}
            <p>{e.label}</p>
          </div>
        </Menu.Item>
      ))}
    </Menu>
  )
}

const Card: React.FC<Props> = (props) => {
  return (
    <Dropdown overlay={<CardOverlay id={props.card.subscribe.id} /> } trigger={['contextMenu']}>
      <div
        onMouseDown={props.onCardMouseDown}
        onMouseUp={props.onCardMouseUp}
        onClick={() => {
          props.onCardClick(props.card.subscribe)
        }}
        onDragStart={(e) => {
          e.dataTransfer.setData('text/plain', props.card.id.toString() + '-' + props.parentColumn.toString())
        }}
        className={classNames(styles.card, !props.card.subscribe.is_subscribed ? styles.unsubscribedCard : null)}
        id={props.card.id.toString()}
        draggable={'true'}>
        <div className={styles.cardHead}>
          <Popconfirm
            title={UITexts.KanbanBoard.SURE_TO_ARCHIVE}
            onCancel={(e) => {
              e?.stopPropagation()
            }}
            onConfirm={(e) => {
              e?.stopPropagation()
              store.dispatch(
                api.endpoints.updateSubscribe.initiate({ id: props.card.subscribe.id, data: { is_archived: true } }),
              )
            }}>
            <FolderAddOutlined
              className={styles.delete}
              onClick={(e) => {
                e.stopPropagation()
              }}
            />
          </Popconfirm>
          {!props.card.subscribe.is_subscribed && (
            <div className={styles.unsubscribed}>
              <p>Unsubscribed</p>
            </div>
          )}
          <h2 className={styles.cardTitle}>
            {props.card.subscribe.user.first_name ?? '-'} {props.card.subscribe.user.last_name ?? '-'}
          </h2>
        </div>
        <div className={styles.cardBody}>
          <p className={styles.cardText}>
            User: <b>{props.card.subscribe.user.email}</b>
          </p>
          <p className={styles.cardText}>
            Product: <b>{props.card.subscribe.product.name}</b>
          </p>
          <p className={classNames(styles.cardText, styles.gray)}>
            {prettyDate(props.card.subscribe.update_at)}
          </p>
        </div>
      </div>
    </Dropdown>
  )
}

export default Card
