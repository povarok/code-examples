import React from 'react'

import Column from './Column'
import {BoardColumn, CardMovedHandler, Subscribe} from './boardTypes';
import styles from './KanbanBoard.module.css'

type Props = {
  columns: BoardColumn[]
  onCardMoved: CardMovedHandler
  onCardClick: (subscribe: Subscribe) => void
  onCardMouseDown: () => void
  onCardMouseUp: () => void
}

const KanbanBoard: React.FC<Props> = (props) => {
  return (
    <div className={styles.main}>
      {props.columns.map((c) => (
        <Column
          key={c.id}
          column={c}
          onCardMoved={props.onCardMoved}
          onCardClick={props.onCardClick}
          onCardMouseUp={props.onCardMouseUp}
          onCardMouseDown={props.onCardMouseDown}
        />
      ))}
    </div>
  )
}

export default KanbanBoard
