import { Subscribe as GlobalSubscribe } from 'typings/entities'

export type Subscribe = GlobalSubscribe;

export type BoardCard = {
  id: number
  subscribe: Subscribe
  [key: string]: any
}

export type BoardColumn = {
  id: number
  name: string
  cards: BoardCard[]
}

export type CardMovedHandler = (fromColumn: number, toColumn: number, cardId: number) => void
