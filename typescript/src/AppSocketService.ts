import openSocket from 'socket.io-client';
import {
  Configuration,
  MutedParticipant,
  AppSocketServiceCfg,
  StreamQuality,
  SDPMessage,
  RestartPCMessage,
  QualityConfigType,
  RequestReplaceTrackMessage,
  CandidateMessage,
  ToggleStreamMessage,
  ToggleRemoteTrackMessage,
} from './types/AppSocketServiceTypes';
import { RTCSessionDescription, RTCIceCandidate } from './WebRTC';
import { isIOSsafari } from './utils'
import { toggleTrack } from './utils/mediaStreamUtils';
import LocalStreamProvider from './LocalStreamProvider';
import RTCPeerConnectionEl, { RTCPeerConnectionElPropsType } from './RTCPeerConnectionEl';

export const API_URL = process.env.API_URL!;

const DEFAULT_SERVERS: Configuration = {
  signalingUrl: process.env.SIGNALING_URL!,
  iceServers: [
    {
      urls: [process.env.TURNSERVER_URL],
      username: process.env.TURNSERVER_USERNAME,
      credential: process.env.TURNSERVER_PASS,
    },
  ],
  bandwidth: {
    screen: 300,
    audio: 48,
    video: 256,
  },
};

const DEFAULT_QUALITY_CONFIG: QualityConfigType = {
  MOBILE: {
    LOW: {
      scale: 4,
      bitrate: 400000,
    },
    HIGH: {
      scale: 2,
      bitrate: 400000,
    },
  },
  DESKTOP: {
    LOW: {
      scale: 2,
      bitrate: 400000,
    },
    HIGH: {
      scale: 1,
      bitrate: 400000,
    },
  },
};

export default class AppSocketService {
  private socket: SocketIOClient.Socket | null;
  private readonly setParticipants: (participants: RTCPeerConnectionEl[]) => void;
  private readonly addMutedParticipant: (participant: MutedParticipant) => void;
  private readonly deleteMutedParticipant: (participant: MutedParticipant) => void;
  private readonly setIsLocalAudioEnabled: (enabled: boolean) => void;
  static configuration: Configuration;
  static qualityConfig: QualityConfigType = DEFAULT_QUALITY_CONFIG;
  stream: MediaStream | null | undefined;
  roomId: string;
  userId: string;
  peerConnections: RTCPeerConnectionEl[];
  pendingUsers: string[];
  requestRoomUsersInterval: ReturnType<typeof setTimeout>;
  webcamStream: MediaStream;
  bandwidth: { screen: number; video: number; audio: number };
  isIOSSafari: boolean;

  constructor(cfg: AppSocketServiceCfg) {
    cfg.setVideoServiceReady(true);
    this.socket = null;

    this.bandwidth = {
      screen: 300,
      audio: 48,
      video: 256,
    };

    this.webcamStream = cfg.localStream;

    this.webcamStream.onremovetrack = () => {
      this.triggerRestartLocalStream();
    };

    this.webcamStream.addEventListener('removetrack', () => {
      this.triggerRestartLocalStream();
    });

    this.webcamStream.getTracks().forEach((track: MediaStreamTrack) => {
      if (track.kind === 'video') {
        track.addEventListener('onmute', () => {
          this.triggerRestartLocalStream();
        });
      }
      track.addEventListener('ended', () => {
        this.triggerRestartLocalStream();
      });
      track.onended = () => {
        this.triggerRestartLocalStream();
      };
      track.addEventListener('inactive', () => {
        this.triggerRestartLocalStream();
      });
    });

    this.roomId = cfg.roomId;
    this.userId = cfg.userId;
    this.setParticipants = cfg.setParticipants;
    this.addMutedParticipant = cfg.addMutedParticipant;
    this.deleteMutedParticipant = cfg.deleteMutedParticipant;
    this.setIsLocalAudioEnabled = cfg.setIsLocalAudioEnabled;
    this.peerConnections = [];
    this.pendingUsers = [];
    this.isIOSSafari = isIOSsafari();

    this.requestRoomUsersInterval = setInterval(() => {
      this.requestRoomUsers();
      __DEV__ && this.updateStats();
      this.healthCheck();
    }, 15_000);

    this.listen();
  }

  async updateStats() {
    const stats = this.peerConnections.map((pcel) => pcel.updateStats());
    await Promise.all(stats);
    this.rerender();
  }

  healthCheck() {
    const now = new Date().getTime();
    for (let pcel of this.peerConnections) {
      if (pcel.stream === undefined) {
        if (pcel.tracksHealth.stream.requestTimestamp! + 10000 < now) {
          pcel.restartPC();
        }
      } else {
        pcel.tracksHealth.stream.requestTimestamp = now;
      }
    }
  }

  requestRoomUsers() {
    this.socket?.emit('request-room-users', { roomId: this.roomId });
  }

  triggerToggleLocalStream(streamType: ('audio' | 'video')[], enabled?: boolean) {
    this.toggleLocalStream(streamType, enabled);
  }

  async triggerRestartLocalStream() {
    LocalStreamProvider.destroyMediaStream();
    const newStream = await LocalStreamProvider.getMediaStream(this.roomId);
    if (newStream === null) {
      setTimeout(() => {
        this.triggerRestartLocalStream();
      }, 1000);
    } else {
      this.webcamStream = newStream;
      if (this.isIOSSafari) {
        this.resendLocalStreamToAll();
      }
    }
  }

  private toggleLocalStream(streamType = ['audio', 'video'], enabled?: boolean) {
    this.peerConnections.forEach(({ webcamStream }) =>
      toggleTrack(webcamStream, streamType, enabled),
    );
    toggleTrack(this.webcamStream, streamType, enabled);
    for (const st of streamType) {
      const currentTrackState = this.webcamStream.getTracks().find((t) => t.kind === st)?.enabled;
      const fields = {
        id: this.userId,
        streamTrack: st,
        trackStatus: currentTrackState,
      };
      this.echoToggleLocalStream(fields);
    }
    if (streamType[0] === 'audio') {
      this.setIsLocalAudioEnabled(!!enabled);
    }
  }

  static getServerConfig(roomId: string): Promise<Configuration> {
    return new Promise(async (resolve) => {
      const reqUrl = `${API_URL}/server_config?roomId=${roomId}`;
      const resp = await fetch(reqUrl)
        .then((r) => r.json())
        .catch((error) => {
          return { error };
        });
      if (resp.error === undefined) {
        resolve(resp);
        // resolve(DEFAULT_SERVERS);
      } else {
        resolve(DEFAULT_SERVERS);
      }
    });
  }

  static getQualityConfig(roomId: string): Promise<QualityConfigType> {
    return new Promise(async (resolve) => {
      const reqUrl = `${API_URL}/new_quality_config?roomId=${roomId}`;
      const resp = await fetch(reqUrl)
        .then((r) => r.json())
        .catch((error) => ({ error }));
      if (resp.error === undefined) {
        resolve(resp);
        // resolve(DEFAULT_QUALITY_CONFIG);
      } else {
        resolve(DEFAULT_QUALITY_CONFIG);
      }
    });
  }

  async listen() {
    AppSocketService.configuration = await AppSocketService.getServerConfig(this.roomId);
    if (AppSocketService.configuration.busy) return;

    AppSocketService.qualityConfig = await AppSocketService.getQualityConfig(this.roomId);

    this.socket = openSocket(AppSocketService.configuration.signalingUrl, {
      query: { userId: this.userId, roomId: this.roomId },
      forceNew: true,
    });

    this.socket.on('room-users', (msg: string[]) => this.handleRoomUsers(msg));
    this.socket.on('new-ice-candidate', (msg: CandidateMessage) =>
      this.handleNewICECandidateMsg(msg),
    );
    this.socket.on('video-offer', (msg: SDPMessage) => this.handleVideoOfferMsg(msg));
    this.socket.on('video-answer', (msg: SDPMessage) => this.handleVideoAnswerMsg(msg));
    this.socket.on('disconnect', () => this.handleDisconnect());
    this.socket.on('user-left', (msg: string) => this.handleUserDisconnect(msg));
    this.socket.on('toggle-stream', (msg: ToggleStreamMessage) =>
      this.toggleLocalStream(['audio'], msg.active),
    );
    this.socket.on('toggle-remote-stream-track', (msg: ToggleRemoteTrackMessage) =>
      this.handleToggleRemoteTrack(msg),
    );
    this.socket.on('request-restart-pc-offer', (msg: RestartPCMessage) =>
      this.handleRequestRestartPc(msg),
    );
    this.socket.on('restart-pc-offer', (msg: RestartPCMessage) => this.handleRestartPcOffer(msg));
    this.socket.on('restart-pc-answer', (msg: RestartPCMessage) => this.handleRestartPcAnswer(msg));
    this.socket.on('request-replace-tracks', (msg: RequestReplaceTrackMessage) =>
      this.handleRequestReplaceTracks(msg),
    );

    this.socket.on('connect', () => {
      if (!this.socket) {
        __DEV__ && console.warn('No socket defined');
        return;
      }
      this.socket.io.opts.query = { userId: this.userId, roomId: this.roomId };
    });

    this.socket.on('reconnect_attempt', () => {
      if (this.socket) {
        this.socket.io.opts.query = { userId: this.userId, roomId: this.roomId };
      }
    });
  }

  close() {
    clearInterval(this.requestRoomUsersInterval);
    this.handleDisconnect();
    this.socket?.removeAllListeners();
    this.socket?.close();
    this.socket = null;
  }

  // signaling handlers
  handleRoomUsers(roomIds: string[]) {
    const connected = this.peerConnections.map((c) => c.target);
    const newConnectionCandidates = roomIds
      .filter((id: string) => !connected.includes(id))
      .filter((id: string) => !this.pendingUsers.includes(id));

    for (let id of newConnectionCandidates) {
      this.invite(id);
    }
  }

  async handleNewICECandidateMsg(msg: CandidateMessage) {
    const candidate = new RTCIceCandidate(msg.candidate);
    const pcel = this.getPC(msg.from);
    if (!pcel) return;

    if (pcel.pc.remoteDescription) {
      await pcel.pc.addIceCandidate(candidate);
    }
  }

  async handleVideoOfferMsg(msg: SDPMessage) {
    const targetId = msg.name;
    let pcel = this.getPC(targetId);
    let isNewConnection = false;
    if (!pcel) {
      pcel = this.createPC(targetId);
      isNewConnection = true;
    }

    pcel.platform = msg.platform;
    const { pc } = pcel;

    const desc = new RTCSessionDescription(msg.sdp);

    const offerCollision = pcel.makingOffer || pc.signalingState !== 'stable';
    pcel.ignoreOffer = !pcel.polite && offerCollision;

    if (pcel.ignoreOffer) {
      return;
    }

    if (offerCollision) {
      await Promise.all([
        pc.setLocalDescription({ type: 'rollback' }),
        pc.setRemoteDescription(desc),
      ]);
      while (pcel.candidatesQueue.length) {
        pcel.candidatesQueue.pop();
      }
    } else {
      await pc.setRemoteDescription(desc);
      while (pcel.candidatesQueue.length) {
        const newCand = pcel.candidatesQueue.pop();
        if (newCand !== undefined) {
          await pc.addIceCandidate(newCand);
        }
      }
    }

    if (isNewConnection) {
      pcel.addPCTracks(['audio', 'video']);
      pcel.setPCResolution();
    }

    const answer = await pc.createAnswer();
    await pc.setLocalDescription(answer).catch(() => null);

    this.socket?.emit('video-answer', {
      name: this.userId,
      target: targetId,
      type: 'video-answer',
      sdp: pc.localDescription,
      platform: LocalStreamProvider.getPlatform(),
    });
  }

  async handleVideoAnswerMsg(msg: SDPMessage) {
    const desc = new RTCSessionDescription(msg.sdp);
    const pcel = this.getPC(msg.name);

    if (pcel) {
      pcel.platform = msg.platform;
      await pcel.pc.setRemoteDescription(desc).catch(() => {});
    }
  }

  handleDisconnect() {
    if (this.socket) {
      this.socket.io.opts.query = { userId: this.userId, roomId: this.roomId };
    }

    for (let pcel of this.peerConnections) {
      pcel.destroy();
    }
    this.peerConnections = [];
    this.setParticipants(this.peerConnections);
  }

  handleUserDisconnect(msg: string) {
    const pcel = this.getPC(msg);
    if (pcel && pcel.pc) {
      pcel.destroy();
      this.removePC(pcel.target);
    }
  }

  handleToggleRemoteTrack(msg: ToggleRemoteTrackMessage) {
    if (msg.trackStatus === true) {
      this.deleteMutedParticipant({ id: msg.id, track: msg.streamTrack });
    } else if (msg.trackStatus === false) {
      this.addMutedParticipant({ id: msg.id, track: msg.streamTrack });
    }
  }

  handleRequestRestartPc(msg: RestartPCMessage) {
    this.socket?.emit('restart-pc-offer', {
      target: msg.from,
      from: msg.target,
    });
  }

  handleRestartPcOffer(msg: RestartPCMessage) {
    if (!this.pendingUsers.includes(msg.from)) {
      this.pendingUsers.push(msg.from);
    }
    this.handleUserDisconnect(msg.from);
    this.socket?.emit('restart-pc-answer', { target: msg.from, from: this.userId });
  }

  handleRestartPcAnswer(msg: RestartPCMessage) {
    this.handleUserDisconnect(msg.from);
    this.invite(msg.from);
  }

  handleRequestReplaceTracks(msg: RequestReplaceTrackMessage) {
    this.getPC(msg.from)?.replacePCTracks(msg.kinds);
  }

  setStreamQuality(quality: StreamQuality) {
    for (let pcel of this.peerConnections) {
      pcel.setPCResolution(quality);
    }
  }

  changeRoom(roomId: string) {
    if (this.socket !== null && this.roomId !== roomId) {
      fetch(`${API_URL}/server_config?roomId=${roomId}&oldRoomId=${this.roomId}`);
      this.socket.emit('request-change-room', { roomId });
      this.roomId = roomId;

      for (let pcel of this.peerConnections) {
        pcel.roomId = this.roomId;
      }
    }
  }

  showParticipant() {
    this.socket?.emit('show-participant');
  }

  echoToggleLocalStream(fields: any) {
    this.socket?.emit('toggle-local-stream-track', fields);
  }

  invite(userId: any) {
    const pcel = this.createPC(userId);
    pcel.addPCTracks(['audio', 'video']);
    pcel.setPCResolution();
  }

  // PC utils
  createPC(targetId: string) {
    if (!AppSocketService.configuration) throw new Error('Configuration is not defined');

    const pcelProps: RTCPeerConnectionElPropsType = {
      targetId,
      socket: this.socket!,
      userId: this.userId,
      webcamStream: this.webcamStream.clone(),
      roomId: this.roomId,
      rerender: this.rerender,
      resendLocalStreamToAll: this.resendLocalStreamToAll,
    };

    const pcel = new RTCPeerConnectionEl(pcelProps);
    this.peerConnections = [...this.peerConnections, pcel];
    this.setParticipants(this.peerConnections);
    this.removePendingUser(targetId);

    return pcel;
  }

  getPC(targetId: string) {
    return this.peerConnections.find((c) => c.target === targetId);
  }

  removePC(targetId: string) {
    const closeIndex = this.peerConnections.findIndex((pce) => pce.target === targetId);
    if (closeIndex !== -1) {
      this.peerConnections.splice(closeIndex, 1);
    }
    this.setParticipants([...this.peerConnections]);
  }

  removePendingUser(targetId: string) {
    const closeIndex = this.pendingUsers.findIndex((userId) => userId === targetId);
    if (closeIndex !== -1) {
      this.pendingUsers.splice(closeIndex, 1);
    }
  }

  resendLocalStreamToAll = () => {
    for (let pcel of this.peerConnections) {
      if (this.isIOSSafari) {
        pcel.webcamStream = this.webcamStream.clone();
      }
      pcel.addPCTracks(['audio', 'video']);
    }
  };

  rerender = () => this.setParticipants([...this.peerConnections]);
}
