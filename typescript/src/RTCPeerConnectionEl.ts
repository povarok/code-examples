import { RTCPeerConnection } from './WebRTC';
import { Platform, StreamQuality, TracksHealth } from './types/AppSocketServiceTypes';
import LocalStreamProvider from './LocalStreamProvider';
import { toggleTrack } from './utils/mediaStreamUtils';
import AppSocketService from './AppSocketService';

export type RTCPeerConnectionElPropsType = {
  targetId: string;
  socket: SocketIOClient.Socket;
  userId: string;
  roomId: string;
  webcamStream: MediaStream;
  rerender: any;
  resendLocalStreamToAll: any;
};
export default class RTCPeerConnectionEl {
  userId: string;
  roomId: string;
  target: string;
  candidatesQueue: RTCIceCandidate[];
  webcamStream: MediaStream | null;
  polite: boolean;
  socket: SocketIOClient.Socket;
  makingOffer: boolean;
  ignoreOffer: boolean;
  pc: RTCPeerConnection;
  stream?: MediaStream;
  toggleState: (streamType: ('audio' | 'video')[], enabled?: boolean) => void;
  rerender: () => void;
  resendLocalStreamToAll: () => void;
  tracksHealth: TracksHealth;
  platform: Platform;
  restarting: boolean;
  stats: any;

  constructor(props: RTCPeerConnectionElPropsType) {
    this.userId = props.userId;
    this.roomId = props.roomId;
    this.target = props.targetId;
    this.webcamStream = props.webcamStream;
    this.candidatesQueue = [];
    this.makingOffer = false;
    this.ignoreOffer = false;
    this.socket = props.socket;
    this.polite = this.userId > this.target;
    this.pc = new RTCPeerConnection({ iceServers: AppSocketService.configuration.iceServers });
    this.platform = 'MOBILE';
    this.rerender = props.rerender;
    this.restarting = false;
    this.resendLocalStreamToAll = props.resendLocalStreamToAll;
    this.toggleState = (streamType = ['audio', 'video'], enabled?: boolean) =>
      this.toggleConnectionStream(streamType, enabled);

    this.restartPC = this.restartPC.bind(this);
    this.toggleState = this.toggleState.bind(this);
    this.tracksHealth = {
      audio: {
        status: null,
        requestTimestamp: null,
      },
      video: {
        status: null,
        requestTimestamp: null,
      },
      stream: {
        status: null,
        requestTimestamp: new Date().getTime(),
      },
    };
    this.stats = {
      target: this.target.substr(this.target.length - 4, 4),
      date: new Date().toISOString(),
    };

    this.pc.onicecandidate = ({ candidate }: any) => {
      if (candidate && this.socket) {
        this.socket.emit('new-ice-candidate', {
          type: 'new-ice-candidate',
          target: this.target,
          from: this.userId,
          candidate,
        });
      }
    };

    this.pc.onnegotiationneeded = async () => {
      try {
        this.makingOffer = true;
        const offer = await this.pc.createOffer();
        if (this.pc.signalingState !== 'stable') return;
        await this.pc.setLocalDescription(offer);
        this.socket?.emit('video-offer', {
          name: this.userId,
          target: this.target,
          type: 'video-offer',
          sdp: this.pc.localDescription,
          platform: LocalStreamProvider.getPlatform(),
        });
      } catch (err) {
      } finally {
        this.makingOffer = false;
      }
    };

    if (this.pc.ontrack === undefined && this.pc.onaddstream === null) {
      this.pc.onaddstream = (e) => {
        this.stream = e.stream;
        this.tracksHealth.stream.status = true;
        this.rerender();
      };
    } else {
      this.pc.ontrack = (e) => {
        this.stream = e.streams[0];
        this.tracksHealth.stream.status = true;
        this.rerender();
      };
    }
  }

  showCodecs(arr: any, dir: string) {
    arr.forEach((tr: any) => {
      if (tr.getParameters !== undefined) {
        const codecs = tr.getParameters().codecs;
        if (tr.track && codecs.length) {
          const kind = tr.track.kind;
          this.stats[`codec_${dir}_${kind}`] = `${codecs[0].mimeType} ${
            codecs[0].sdpFmtpLine || ''
          }`;
        }
      }
    });
  }

  updateStats() {
    this.pc.getSenders !== undefined && this.showCodecs(this.pc.getSenders(), 'out');
    this.pc.getReceivers !== undefined && this.showCodecs(this.pc.getReceivers(), 'in');

    return new Promise<void>(async (resolve) => {
      if (this.pc.iceConnectionState !== 'connected') {
        return;
      }
      try {
        const stats = await this.pc.getStats();
        stats.forEach((stat) => {
          if (!stat.isRemote) {
            switch (stat.type) {
              case 'outbound-rtp': {
                this.stats.outboundRtp_outbound = this.dumpOutbound(stat);
                let remote = stats.get(stat.remoteId);
                if (remote) this.stats.outboundRtp_inbound = this.dumpInbound(remote);
                break;
              }
              case 'inbound-rtp': {
                this.stats.inboundRtp_inbound = this.dumpInbound(stat);
                let remote = stats.get(stat.remoteId);
                if (remote) this.stats.inboundRtp_outbound = this.dumpOutbound(remote);
                break;
              }
            }
          }
        });
      } catch (e) {
        __DEV__ && console.log(e);
      }

      const now = new Date().toISOString();
      this.stats.date = now;
      resolve();
    });
  }

  mb = (bytes: number) => (bytes / 1024000).toFixed(2);
  dumpOutbound = (o: any) =>
    `Sent: ${o.packetsSent} packets (${this.mb(o.bytesSent)} MB){}
     Dropped frames: ${o.droppedFrames}`;
  dumpInbound = (o: any) =>
    `Received: ${o.packetsReceived} packets (${this.mb(o.bytesReceived)} MB){}
     Lost: ${o.packetsLost}{}
     Discarded packets: ${o.discardedPackets} Jitter: ${o.jitter}`;

  applyCodecPreferences(transceiver: RTCRtpTransceiver) {
    // @ts-ignore
    // eslint-disable-next-line no-undef
    if (RTCRtpSender.getCapabilities && transceiver.setCodecPreferences) {
      // @ts-ignore
      // eslint-disable-next-line no-undef
      const codecs = RTCRtpSender.getCapabilities('video').codecs.filter(
        (c) => c.mimeType === 'video/H264',
      );
      transceiver.setCodecPreferences(codecs);
    }
  }

  replacePCTracks() {
    this.clearPCTracks();
    this.addPCTracks();
  }

  async setPCResolution(quality: StreamQuality = 'LOW') {
    if (this.pc.getSenders === undefined) return;
    const senders = this.pc.getSenders();
    const videoSender = senders?.find((s) => s.track?.kind === 'video');
    const targetPlatform =
      LocalStreamProvider.getPlatform() === 'MOBILE' ? 'MOBILE' : this.platform;
    const { scale, bitrate } = AppSocketService.qualityConfig[targetPlatform][quality];

    if (videoSender && videoSender.setParameters) {
      const params = videoSender.getParameters();

      if (!params.encodings.length) {
        setTimeout(() => {
          this.setPCResolution(quality);
        }, 500);
        return;
      }
      if (!params.encodings) params.encodings = [{}]; // Firefox workaround!

      params.encodings[0].scaleResolutionDownBy = scale;
      params.encodings[0].maxBitrate = bitrate;
      await videoSender.setParameters(params);
    }
  }

  async addPCTracks() {
    if (this.webcamStream) {
      if (this.pc.addTrack !== undefined) {
        for (let track of this.webcamStream.getTracks()) {
          const needReplace = this.pc
            .getSenders()
            .find((s: any) => (s.track ? s.track.kind === track.kind : false));
          if (needReplace) {
            const sender = this.pc.getSenders().find((s) => s.track?.kind === track.kind);
            if (sender) {
              sender.replaceTrack(track);
            }
          } else {
            this.pc.addTrack(track, this.webcamStream!);
          }
        }
        //@ts-ignore
      } else if (this.pc.addStream !== undefined) {
        //@ts-ignore
        if (this.pc.getLocalStreams().length > 0 && this.webcamStream !== null) {
          //@ts-ignore
          this.pc.removeStream(this.webcamStream);
        }
        //@ts-ignore
        this.pc.addStream(this.webcamStream);
      }
    }
  }

  restartPC() {
    if (this.restarting) return;
    this.restarting = true;
    if (this.polite) {
      this.socket?.emit('request-restart-pc-offer', {
        target: this.target,
        from: this.userId,
      });
    } else {
      this.socket?.emit('restart-pc-offer', {
        target: this.target,
        from: this.userId,
      });
    }
  }

  clearPCTracks() {
    if (this.pc.getSenders !== undefined) {
      for (let sender of this.pc.getSenders()) {
        this.pc.removeTrack(sender);
      }
    } else if (this.webcamStream !== null) {
      // @ts-ignore
      this.pc.removeStream(this.webcamStream);
    }
  }

  handleLocalTrackEnded = () => {
    setTimeout(async () => {
      LocalStreamProvider.destroyMediaStream();
      const newStream = await LocalStreamProvider.getMediaStream(this.roomId);
      if (newStream === null) {
        this.handleLocalTrackEnded();
      } else {
        this.webcamStream = newStream;
        this.resendLocalStreamToAll();
      }
    }, 1000);
  };

  toggleConnectionStream(streamType = ['audio', 'video'], enabled?: boolean) {
    if (this.stream) {
      toggleTrack(this.stream, streamType, enabled);
    }
  }

  destroy() {
    this.toggleState = () => {};
    this.clearPCTracks();
    this.webcamStream?.getTracks().forEach((track) => {
      track.stop();
      this.webcamStream?.removeTrack(track);
    });
    this.pc.close();
  }
}
