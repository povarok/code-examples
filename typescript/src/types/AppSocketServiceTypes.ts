import RTCPeerConnectionEl from '../RTCPeerConnectionEl';

export type MutedParticipant = {
  id: string;
  track: string;
};

export type QualityConfigType = {
  [id: string]: {
    [id: string]: {
      scale: number;
      bitrate: number;
    };
  };
};

export type TracksHealth = {
  [audio: string]: {
    status: true | null | 'pending';
    requestTimestamp: number | null;
  };
};

export type TrackKind = 'audio' | 'video';

export type Platform = 'DESKTOP' | 'MOBILE';

export type StreamQuality = 'HIGH' | 'LOW';

export type Bandwidth = {
  screen: number;
  audio: number;
  video: number;
};

export type Configuration = {
  signalingUrl: string;
  iceServers: any[];
  bandwidth: Bandwidth;
  busy?: boolean;
};

export type AppSocketServiceCfg = {
  localStream: MediaStream;
  roomId: string;
  userId: string;
  setVideoServiceReady: (ready: boolean) => void;
  setParticipants: (participants: RTCPeerConnectionEl[]) => void;
  addMutedParticipant: (participant: MutedParticipant) => void;
  deleteMutedParticipant: (participant: MutedParticipant) => void;
  setIsLocalAudioEnabled: (enabled: boolean) => void;
};

export type SDPMessage = {
  name: string;
  target: string;
  type: string;
  sdp: string;
  platform: Platform;
};

export type CandidateMessage = {
  type: string;
  candidate: RTCIceCandidateType;
  target: string;
  from: string;
};

export type ToggleStreamMessage = {
  active: boolean
};

export type ToggleRemoteTrackMessage = {
  id: string;
  streamTrack: string;
  trackStatus: boolean;
};

export type RestartPCMessage = {
  from: string;
  target: string;
};

export type RequestReplaceTrackMessage = {
  from: string;
  target: string;
  kinds: string[];
};
