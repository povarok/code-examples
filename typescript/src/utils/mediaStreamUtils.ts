export function toggleTrack(stream: MediaStream | null, type: string[], enabled?: boolean) {
  stream?.getTracks().forEach((track) => {
    type.forEach((t) => {
      if (track.kind === t) {
        track.enabled = enabled ?? !track.enabled;
      }
    });
  });
}

export function setVP8codec(sdp: string): string {
  function getVP8Number(SDP: string): string {
    const lines = SDP.split('\r\n');
    for (let line of lines) {
      if (line.indexOf('VP8') !== -1) {
        return line.split(':')[1].split(' ')[0];
      }
    }
    return '96';
  }

  const lines = sdp.split('\r\n');
  let newSdp = '';
  const codecNum = getVP8Number(sdp);

  let isVideoConf = false;
  for (let j = 0; j < lines.length - 1; ++j) {
    if (lines[j].startsWith('m=audio')) {
      isVideoConf = false;
    }
    if (lines[j].startsWith('m=video')) {
      isVideoConf = true;
      const codecLine = lines[j].split(' ');
      for (let i = codecLine.length - 1; i >= 0; --i) {
        if (!isNaN(parseInt(codecLine[i]))) {
          if (codecLine[i] !== codecNum) {
            codecLine.splice(i, 1);
          }
        } else {
          break;
        }
      }
      lines[j] = codecLine.join(' ');
    } else if (isVideoConf) {
      if (
        (lines[j].startsWith('a=rtpmap') && !lines[j].split(':')[1].startsWith(codecNum)) ||
        (lines[j].startsWith('a=rtcp-fb') && !lines[j].split(':')[1].startsWith(codecNum)) ||
        lines[j].startsWith('a=fmtp')
      ) {
        lines.splice(j, 1);
        j--;
      }
    }
    newSdp += lines[j] + '\r\n';
  }

  return newSdp;
}

